#!/usr/bin/env python
# coding: utf-8

# In[1]:


pip install matplotlib


# In[1]:


import matplotlib.pyplot as plt


# In[2]:


get_ipython().run_line_magic('matplotlib', 'inline')


# In[4]:


import numpy as np
x = np.linspace(0,5,11)
y =x**2


# In[5]:


x


# In[6]:


y


# In[7]:


plt.plot(x,y,'r')
plt.xlabel('X Axis Title Here')
plt.ylabel('Y Axis Title Here')
plt.title('String Title Here')
plt.show()


# In[8]:


plt.subplot(1,2,1)
plt.plot(x,y,'r--')
plt.subplot(1,2,2)
plt.plot(y,x,'g*-');


# In[9]:


fig = plt.figure()
axes=fig.add_axes([0.1,0.1,0.8,0.8])
axes.plot(x,y,'b')
axes.set_xlabel('Set X Label')
axes.set_ylabel('Set Y Label')
axes.set_title('Set Title')


# In[12]:


fig = plt.figure()

axes1=fig.add_axes([0.1,0.1,0.8,0.8])
axes2=fig.add_axes([0.2,0.5,0.4,0.3])

axes1.plot(x,y,'b')
axes1.set_xlabel('X_label_axes2')
axes1.set_ylabel('Y_label_axes2')
axes1.set_title('Axes 2 Title')

axes2.plot(y,x,'r')
axes2.set_xlabel('X_label_axes2')
axes2.set_ylabel('Y_label_axes2')
axes2.set_title('Axes 2 Title')


# In[13]:


fig, axes = plt.subplots()

axes.plot(x,y,'r')
axes.set_xlabel('x')
axes.set_ylabel('y')
axes.set_title('title')


# In[14]:


fig, axes = plt.subplots(nrows=1,ncols=2)


# In[15]:


axes


# In[16]:


for ax in axes:
    ax.plot(x,y,'b')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_title('title')
    
fig


# In[17]:


fig, axes =plt.subplots(nrows=1, ncols=2)

for ax in axes:
    ax.plot(x,y,'g')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_title('title')
    
fig
plt.tight_layout()


# In[18]:


fig = plt.figure(figsize=(8,4), dpi=100)


# In[20]:


fig, axes=plt.subplots(figsize=(12,3))

axes.plot(x,y,'r')
axes.set_xlabel('x')
axes.set_ylabel('y')
axes.set_title('title');


# In[22]:


fig.savefig("filename.png")


# In[23]:


fig.savefig("filename.png", dpi=200)


# In[24]:


ax.set_title("title")


# In[25]:


ax.set_xlabel("x")
ax.set_ylabel("y");


# In[26]:


fig = plt.figure()

ax=fig.add_axes([0,0,1,1])

ax.plot(x, x**2, label="x**2")
ax.plot(x, x**3, label="x**3")
ax.legend()


# In[27]:


ax.legend(loc=1)
ax.legend(loc=2)
ax.legend(loc=3)
ax.legend(loc=4)

ax.legend(loc=0)
fig


# In[28]:


fig, ax=plt.subplots()
ax.plot(x,x**2,'b.-')
ax.plot(x,x**3,'g--')


# In[29]:


fig, ax=plt.subplots()

ax.plot(x,x+1, color="blue", alpha=0.5)
ax.plot(x,x+2,color="#8B008B")
ax.plot(x,x+3,color="#FF8C00")


# In[36]:


fig , ax =plt.subplots(figsize=(12,6))

ax.plot(x, x+1, color="red", linewidth=0.25)
ax.plot(x, x+2, color="red", linewidth=0.50)
ax.plot(x, x+3, color="red", linewidth=1.00)
ax.plot(x, x+4, color="red", linewidth=2.00)

ax.plot(x, x+5, color="green", lw=3, linestyle='-')
ax.plot(x, x+6, color="green", lw=3, ls='-.')
ax.plot(x, x+7, color="green", lw=3, ls=':')

line, = ax.plot(x, x+8, color="black", lw=1.50)
line.set_dashes([5, 10, 15, 10])

ax.plot(x, x+ 9, color="blue", lw=3, ls='-', marker='+')
ax.plot(x, x+10, color="blue", lw=3, ls='--', marker='o')
ax.plot(x, x+11, color="blue", lw=3, ls='-', marker='s')
ax.plot(x, x+12, color="blue", lw=3, ls='--', marker='1')

ax.plot(x, x+13, color="purple", lw=1, ls='-', marker='o', markersize=2)
ax.plot(x, x+14, color="purple", lw=1, ls='-', marker='o', markersize=4)
ax.plot(x, x+15, color="purple", lw=1, ls='-', marker='o', markersize=8, markerfacecolor = "red")
ax.plot(x, x+16, color="purple", lw=1, ls='-', marker='s', markersize=8, markerfacecolor="yellow", markeredgewidth=3, markeredgecolor="green");


# In[39]:


fig, axes = plt.subplots(1,3,figsize=(12,4))

axes[0].plot(x, x**2, x, x**3)
axes[0].set_title("default axes ranges")

axes[1].plot(x, x**2, x, x**3)
axes[1].axis('tight')
axes[1].set_title("tight axes")

axes[2].plot(x, x**2, x, x**3)
axes[2].set_ylim([0,60])
axes[2].set_xlim([2,5])
axes[2].set_title("custom axes range");


# In[40]:


plt.scatter(x,y)


# In[41]:


from random import sample
data=sample(range(1,1000),100)
plt.hist(data)


# In[42]:


data=[np.random.normal(0, std,100) for std in range(1,4)]
plt.boxplot(data,vert=True,patch_artist=True);


# In[3]:


import numpy as np
import pandas as pd
get_ipython().run_line_magic('matplotlib', 'inline')


# In[4]:


df1=pd.read_csv('df1',index_col=0)
df2=pd.read_csv('df2')


# In[62]:


df1['A'].hist()


# In[75]:


import matplotlib.pyplot as plt
plt.style.use('ggplot')


# In[47]:


df1['A'].hist()


# In[48]:


plt.style.use('bmh')
df1['A'].hist()


# In[49]:


plt.style.use('dark_background')
df1['A'].hist()


# In[50]:


plt.style.use('fivethirtyeight')
df1['A'].hist()


# In[51]:


df1['A'].hist()


# In[52]:


plt.style.use('ggplot')


# In[53]:


df2.head()


# In[54]:


df2.plot.bar()


# In[55]:


df2.plot.bar(stacked=True)


# In[56]:


df2.plot.area(alpha=0.4)


# In[59]:


df1['A'].plot.hist(bins=50)


# In[10]:


df1['Year']=df1.index
df1.plot.line(x='Year',y='B',figsize=(12,3),lw=1)


# In[66]:


df1.plot.scatter(x='A',y='B')


# In[68]:


df1.plot.scatter(x='A',y='B',c='C',cmap='coolwarm')


# In[69]:


df1.plot.scatter(x='A',y='B',s=df1['C']*200)


# In[70]:


df2.plot.box()


# In[71]:


df=pd.DataFrame(np.random.randn(1000,2),columns=['a','b'])
df.plot.hexbin(x='a',y='b',gridsize=25,cmap='Oranges')


# In[1]:


import scipy


# In[5]:


df2['a'].plot.kde()


# In[6]:


df2.plot.density()


# In[7]:


import pandas as pd
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')


# In[9]:


mcdon=pd.read_csv('mcdonalds.csv',index_col='Date',parse_dates=True)


# In[11]:


mcdon.head()


# In[12]:


mcdon.plot()


# In[13]:


mcdon['Adj. Close'].plot()


# In[14]:


mcdon['Adj. Volume'].plot()


# In[15]:


mcdon['Adj. Close'].plot(figsize=(12,8))


# In[16]:


mcdon['Adj. Close'].plot(figsize=(12,8))
plt.ylabel('Close Price')
plt.xlabel('Overwrite Date Index')
plt.title('Mcdonalds')


# In[17]:


mcdon['Adj. Close'].plot(figsize=(12,8),title='Pandas Title')


# In[18]:


mcdon['Adj. Close'].plot(xlim=['2007-01-01','2009-01-01'])


# In[22]:


mcdon['Adj. Close'].plot(xlim=['2007-01-01','2009-01-01'],ylim=[0,50])


# In[23]:


mcdon['Adj. Close'].plot(xlim=['2007-01-01','2007-05-01'],ylim=[0,40],ls='--',c='r')


# In[24]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as dates


# In[26]:


mcdon['Adj. Close'].plot(xlim=['2007-01-01','2007-05-01'],ylim=[0,40])


# In[27]:


idx=mcdon.loc['2007-01-01':'2007-05-01'].index
stock=mcdon.loc['2007-01-01':'2007-05-01']['Adj. Close']


# In[28]:


idx


# In[29]:


stock


# In[30]:


fig, ax=plt.subplots()
ax.plot_date(idx,stock,'-')
plt.tight_layout()
plt.show


# In[32]:


fig, ax=plt.subplots()
ax.plot_date(idx,stock,'-')

fig.autofmt_xdate()
plt.tight_layout()
plt.show()


# In[33]:


fig, ax=plt.subplots()
ax.plot_date(idx,stock,'-')
ax.yaxis.grid(True)
ax.xaxis.grid(True)
fig.autofmt_xdate()
plt.tight_layout()
plt.show()


# In[37]:


fig, ax=plt.subplots()
ax.plot_date(idx,stock,'-')

ax.yaxis.grid(True)
ax.xaxis.grid(True)

ax.xaxis.set_major_locator(dates.MonthLocator())
ax.xaxis.set_major_formatter(dates.DateFormatter('%b\n%Y'))
fig.autofmt_xdate()
plt.tight_layout()
plt.show()


# In[39]:


fig, ax=plt.subplots()
ax.plot_date(idx,stock,'-')

ax.yaxis.grid(True)
ax.xaxis.grid(True)

ax.xaxis.set_major_locator(dates.MonthLocator())
ax.xaxis.set_major_formatter(dates.DateFormatter('\n\n\n\n%Y--%B'))

fig.autofmt_xdate()
plt.tight_layout()
plt.show()


# In[40]:


fig, ax=plt.subplots()
ax.plot_date(idx,stock,'-')

ax.xaxis.set_major_locator(dates.MonthLocator())
ax.xaxis.set_major_formatter(dates.DateFormatter('\n\n%Y--%B'))

ax.xaxis.set_minor_locator(dates.WeekdayLocator())
ax.xaxis.set_minor_formatter(dates.DateFormatter('%d'))

ax.yaxis.grid(True)
ax.xaxis.grid(True)

fig.autofmt_xdate()
plt.tight_layout()
plt.show()


# In[42]:


fig, ax =plt.subplots(figsize=(10,8))
ax.plot_date(idx,stock,'-')

ax.xaxis.set_major_locator(dates.WeekdayLocator(byweekday=1))
ax.xaxis.set_major_formatter(dates.DateFormatter('%B-%d-%a'))

ax.yaxis.grid(True)
ax.xaxis.grid(True)

fig.autofmt_xdate()

plt.tight_layout()
plt.show()


# In[ ]:




