#!/usr/bin/env python
# coding: utf-8

# In[3]:


pip install matplotlib


# In[1]:


import numpy as np
x=np.arange(0,100)
y=x*2
z=x**2


# In[2]:


import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')

plt.show()


# In[6]:


plt.plot(x,y)
plt.xlabel('X')
plt.ylabel('Y')
plt.title('title')
plt.show()


# In[29]:


print('hi')


# In[3]:


fig=plt.figure()

axes1=fig.add_axes([0,0,1,1])
axes2=fig.add_axes([0.2,0.5,.2,.2])


# In[5]:



axes1.plot(x,y,'r')
axes1.set_xlabel('x')
axes1.set_ylabel('y')
axes1.set_title('')

axes2.plot(x,y,'r')
axes2.set_xlabel('x')
axes2.set_ylabel('y')
axes2.set_title('')
fig 


# In[45]:


fig=plt.figure()

axes1=fig.add_axes([0,0,1,1])
axes2=fig.add_axes([0.2,0.5,.4,.4])


# In[46]:


axes1.plot(x,z)
axes1.set_xlabel('x')
axes1.set_ylabel('z')

axes2.plot(x,y)
axes2.set_xlabel('x')
axes2.set_ylabel('y')
axes2.set_xlim((20,22))
axes2.set_ylim((30,50))
axes2.set_title('zoom')
fig 


# In[41]:


fig, axes=plt.subplots(nrows=1, ncols=2)


# In[35]:


import numpy as np
import matplotlib.pyplot as plt

plt.subplot(1,2,1)
plt.plot(x,z,'r')

plt.subplot(1,2,2)
plt.plot(x,y,'b--')

plt.show()


# In[40]:


import numpy as np
import matplotlib.pyplot as plt

plt.subplots(figsize=(12,2))

plt.subplot(1,2,1)
plt.plot(x,z,'r--')

plt.subplot(1,2,2)
plt.plot(x,y,'b')

plt.show()


# In[47]:


import pandas as pd
import matplotlib.pyplot as plt
df3=pd.read_csv('df3')
get_ipython().run_line_magic('matplotlib', 'inline')


# In[48]:


df3.info()


# In[49]:


df3.head()


# In[72]:


df3.plot.scatter(figsize=(12,3),x='a',y='b')


# In[78]:


df3['a'].hist()


# In[79]:


import matplotlib.pyplot as plt
plt.style.use('ggplot')


# In[89]:


df3['a'].plot.hist(bins=25)


# In[93]:


df3.boxplot(column=['a','b'])


# In[112]:


df3['d'].plot.kde(lw=5,ls='--')


# In[143]:


df=df3.head(31)
df.plot.area()

